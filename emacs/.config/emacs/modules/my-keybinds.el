(provide 'my-keybinds)

(use-package general
  :ensure t
  :after evil
  :config
  (general-auto-unbind-keys))

;; Leader setup
(general-create-definer my/localleader
  :states '(normal visual insert emacs)
  :keymaps 'override
  :prefix ","
  :major-modes t
  :non-normal-prefix "C-,")             ; This does not work
(my/localleader "" nil)

(general-create-definer my/leader
  :states '(normal visual insert emacs)
  :keymaps 'override
  :prefix "SPC"
  :non-normal-prefix "M-SPC")

;; Hacks for localleader
;; SPC m
(my/leader
  :state 'normal
  "" nil
  "m" (general-simulate-key "," :which-key "local leader"))

;; C-,
(general-define-key
 :states '(insert emacs)
 :keymaps 'override
 "C-," (general-simulate-key "," :state 'normal))

;; A hack for separate TAB and C-i when possible
(if (display-graphic-p)
    (define-key input-decode-map "\C-i" [C-i]))

;; Maps setup
(define-prefix-command 'my/buffer-map)
(define-prefix-command 'my/code-map)
(define-prefix-command 'my/debug-map)
(define-prefix-command 'my/file-map)
(define-prefix-command 'my/git-map)
(define-prefix-command 'my/insert-map)
(define-prefix-command 'my/jump-map)
(define-prefix-command 'my/open-map)
(define-prefix-command 'my/org-map)
(define-prefix-command 'my/project-map)
(define-prefix-command 'my/quit-map)
(define-prefix-command 'my/search-map)
(define-prefix-command 'my/tab-map)
(define-prefix-command 'my/window-map)
(define-prefix-command 'my/yank-map)

;; Maps keybinds
(my/leader
  "'" #'vertico-repeat
  "," #'consult-project-buffer
  "." #'find-file
  ";" 'pp-eval-expression
  "<" #'consult-buffer
  "SPC" #'project-find-file
  "TAB" 'my/tab-map
  "X" 'org-capture
  "b" 'my/buffer-map
  "c" 'my/code-map
  "d" 'my/debug-map
  "f" 'my/file-map
  "g" 'my/git-map
  "i" '("Insert" . my/insert-map)
  "j" 'my/jump-map
  "h" help-map
  "n" '("Organizing" . my/org-map)
  "o" 'my/open-map
  "p" 'my/project-map
  "q" 'my/quit-map
  "s" 'my/search-map
  "t" 'my/toggle-transient
  "u" #'universal-argument
  "w" 'my/window-map
  "y" 'my/yank-map)

(general-define-key
 :keymaps 'my/org-map
 "d" #'narrow-to-defun
 "r" #'narrow-to-region
 "w" #'widen)

(general-define-key
 :keymaps 'my/org-map
 :prefix "D"
 "N" '(my/denote-subdirectory-new :which-key "new note in new subdirectory")
 "n" #'my/denote-subdirectory :desc "new note in existing subdirecory"
 "D" #'my/denote-subdirectory-with-date :desc "new note in existing subdirecory with date"
 "d" #'my/denote :desc "new note in main notes dir"
 "b" #'my/denote-browse :desc "browse notes in denote directory")

;; Insert mode main keybinds
(general-define-key
 :states 'insert
 "C-a" #'move-beginning-of-line
 "C-e" #'move-end-of-line
 "C-p" #'evil-complete-previous
 "C-n" #'evil-complete-next
 "C-k" #'kill-line
 "C-h" help-map
 "C-u" #'universal-argument
 "C-SPC" #'completion-at-point
 "C-S-SPC" #'comint-dynamic-complete-filename
 "M-<return>" '("Slurp" . (lambda () (interactive) (kill-forward-chars 1) (forward-sexp) (yank) (backward-char)))
 )

;; Insert mode minibuffer and shell keybinds
(general-define-key
 :keymaps '(minibuffer-mode-map);; vterm-mode)
 "C-u" '("kill line backwards" . (lambda (arg) (interactive "p") (kill-line (- 1 arg)))))

;; All modes
(general-define-key
 :keymaps '(normal insert visual motion)
 "M-." #'vertico-repeat
 "C-w C-w" #'ace-window
 "M-y" #'consult-yank-from-kill-ring
 "M-1" '("Tab 1" . (lambda () (interactive) (tab-bar-select-tab 1)))
 "M-2" '("Tab 2" . (lambda () (interactive) (tab-bar-select-tab 2)))
 "M-3" '("Tab 3" . (lambda () (interactive) (tab-bar-select-tab 3)))
 "M-4" '("Tab 4" . (lambda () (interactive) (tab-bar-select-tab 4)))
 "M-5" '("Tab 5" . (lambda () (interactive) (tab-bar-select-tab 5)))
 "M-6" '("Tab 6" . (lambda () (interactive) (tab-bar-select-tab 6)))
 "M-7" '("Tab 7" . (lambda () (interactive) (tab-bar-select-tab 7)))
 "M-8" '("Tab 8" . (lambda () (interactive) (tab-bar-select-tab 8)))
 "M-9" '("Tab 9" . (lambda () (interactive) (tab-bar-select-tab 9))))

;; Normal/visual mode main keybinds
(general-define-key
 :keymaps '(normal visual)
 "TAB" #'evil-jump-item
 "<C-i>" #'better-jumper-jump-forward
 "gc" #'evilnc-comment-operator
 "gd" #'xref-find-definitions
 "gD" #'xref-find-definitions-other-window
 "gr" #'xref-find-references
 "C-w gd" #'xref-find-definitions-other-window
 "gl" #'evil-lion-left
 "gL" #'evil-lion-right
 "gj" '("Next fold" . origami-next-fold)
 "gk" '("Previous fold" . origami-previous-fold)
 "C-a" #'evil-numbers/inc-at-pt
 "C-q" #'evil-numbers/dec-at-pt ;; C-x is so important in emacs
 "g=" #'evil-numbers/inc-at-pt-incremental
 "g-" #'evil-numbers/dec-at-pt-incremental
 "M-]" 'er/expand-region
 "M-[" 'er/contract-region
 "M-d" #'evil-mc-make-and-goto-next-match
 "M-D" #'evil-mc-make-and-goto-prev-match
 "C-<down-mouse-2>" (lambda (event) (interactive "e") (evil-mc-toggle-cursor-on-click event))
 "<escape>" #'my/global-escape
 "C-," #'evil-repeat-pop-next
 )

;; Visual mode main keybinds
(general-define-key
 :keymaps 'visual
 ;; keep selection when (un)indenting
 "<" (lambda () (interactive) (call-interactively #'evil-shift-left) (evil-normal-state) (evil-visual-restore))
 ">" (lambda () (interactive) (call-interactively #'evil-shift-right) (evil-normal-state) (evil-visual-restore))
 )

(general-define-key
 :states 'normal
 "M-n" #'drag-stuff-down
 "M-p" #'drag-stuff-up)

;; Normal/motion mode main keybinds
(general-define-key
 :states '(motion normal)
 [remap evil-jump-backward] #'better-jumper-jump-backward
 [remap evil-jump-forward] #'better-jumper-jump-forward
 "]a" #'evil-forward-arg
 "[a" #'evil-backward-arg
 ;; compilation is similiar to vim's quickfix list.
 "]q" #'next-error
 "[q" #'previous-error
 "[b" #'evil-prev-buffer
 "]b" #'evil-next-buffer)

;; Override keymaps
(general-define-key
 :keymaps 'override
 "M-s" 'my/search-map
 "C-x p" 'my/project-map)

(general-define-key
 :keymaps 'override
 :prefix "C-x"
 "u" 'undo-tree-visualize
 "M-:"  #'consult-complex-command
 "b"  #'consult-buffer
 "4 b" #'consult-buffer-other-window
 "5 b" #'consult-buffer-other-frame
 "r b" #'consult-bookmark
 "p b" #'consult-project-buffer)

(general-define-key
 :keymaps 'override
 :prefix "M-g"
 "e" #'consult-compile-error
 "f" #'consult-flymake
 "g" #'consult-goto-line
 "M-g" #'consult-goto-line
 "o" #'consult-outline
 "m" #'consult-mark
 "k" #'consult-global-mark
 "i" #'consult-imenu
 "I" #'consult-imenu-multi)

(general-define-key
 :keymaps 'override
 :prefix "C-c"
 "M-x" #'consult-mode-command
 "h" #'consult-history
 "k" #'consult-kmacro
 "m" #'consult-man
 "i" #'consult-info
 "M-#" #'consult-register-load
 "M-'" #'consult-register-store
 "C-M-#" #'consult-register
 "M-y" #'consult-yank-pop)

(general-define-key
 :keymaps '(override minibuffer-local-map)
 :prefix "C-c"
 "M-s" #'consult-history
 "M-r" #'consult-history)

;; Dired/dirvish keybinds
(my/localleader
  :keymaps 'dirvish-mode-map
  "d" #'dirvish-dispatch
  "b" #'dirvish-goto-bookmark
  "z" #'dirvish-show-history
  "f" #'dirvish-file-info-menu
  "F" #'dirvish-toggle-fullscreen
  "l" #'dired-find-file
  "h" #'dired-up-directory
  "D" #'my/dired-delete-permanently
  "h" #'dired-omit-mode)

(general-define-key
 :keymaps 'dirvish-mode-map
 :states 'normal
 "h" #'dired-up-directory
 "l" #'dired-find-file
 "F" #'dirvish-layout-toggle
 "f" #'dirvish-fd
 "b" #'dirvish-quick-access :which-key "bookmarks"
 "TAB" #'dirvish-subtree-toggle)

(general-define-key
 :keymaps 'my/jump-map
 "o" #'better-jumper-jump-backward
 "i" #'better-jumper-jump-forward)

;; HTML keybinds
(my/localleader
  :keymaps 'html-mode-map
  "p" #'my/open-buffer-with-xwidget)

;; Project map
(general-define-key
 :keymaps 'my/project-map
 "c" #'project-compile
 "p" #'project-switch-project
 "s" #'project-shell
 "e" #'project-eshell
 "!" #'project-shell-command
 "." #'project-find-file
 "t" #'my/project-todos)

;; Yank map
(general-define-key
 :keymaps 'my/yank-map
 "l" '("copy lines" . avy-copy-line)
 "r" '("copy region" . avy-copy-region)
 "p" '("copy file full path" . (lambda () (interactive) (kill-new (buffer-file-name))))
 "f" '("copy filename" . (lambda () (interactive) (kill-new (file-name-nondirectory (buffer-file-name)))))
 "d" '("copy parent directory path" . (lambda () (interactive) (kill-new (file-name-directory (buffer-file-name)))))
 "t" '("append region to target buffer" . my/append-to-visible-buffer))

;; Search map
(general-define-key
 :keymaps 'my/search-map
 "p" #'consult-ripgrep
 "s" #'consult-line
 "d" #'consult-locate
 "l" #'consult-line-multi
 "h" #'consult-isearch-history
 "d" #'consult-find
 "i" #'consult-imenu
 "j" #'consult-mark
 "J" #'consult-global-mark
 "L" #'consult-locate
 "g" #'consult-grep
 "G" #'consult-git-grep
 "r" #'consult-ripgrep
 "l" #'consult-line
 "\"" #'consult-register
 "D" '("find in other directory" . (lambda () (interactive) (let ((current-prefix-arg 4)) (call-interactively #'consult-find))))
 "P" '("ripgrep in other directory" . (lambda () (interactive) (let ((current-prefix-arg 4)) (call-interactively #'consult-ripgrep))))
 "k" #'consult-keep-lines
 "u" #'consult-focus-lines
 "e" #'consult-isearch-history
 "SPC" #'avy-goto-char-timer)

;; Search map -> avy
(general-define-key
 :keymaps 'my/search-map
 :infix "a"
 "l" #'avy-goto-line
 "L" #'avy-move-line
 "R" #'avy-move-region
 "s" #'avy-goto-char-timer
 "n" #'avy-next
 "p" #'avy-prev
 "w" #'avy-goto-whitespace-end
 "h" #'avy-org-goto-heading-timer)

;; Code map
(general-define-key
 :keymaps 'my/code-map
 "f" #'format-all-region-or-buffer
 "c" #'compile
 "C" #'recompile
 "K" #'kill-compilation
 "k" #'eldoc-print-current-symbol-info
 "e" #'consult-flymake
 "D" #'xref-find-references
 "[" #'xref-go-back
 "]" #'xref-go-forward
 "e" #'consult-flymake)

;; Debug map
(general-define-key
 :keymaps 'my/debug-map
 "SPC" #'my/debug-transient
 "s"   #'dape
 "n"   #'dape-next
 "i"   #'dape-step-in
 "o"   #'dape-step-out
 "c"   #'dape-continue
 "r"   #'dape-restart
 "b"   #'dape-breakpoint-toggle
 "d"   #'dape-remove-breakpoint-at-point
 "D"   #'dape-breakpoint-remove-all
 "l"   #'dape-breakpoint-log
 "q"   #'dape-quit
 "k"   #'dape-kill)

 ;; Dape map
(general-define-key
 :states '(normal motion)
 :keymaps 'dape-info-scope-mode-map
 "TAB" #'dape-info-scope-toggle
 )

;; Transient map
(general-define-key
 :keymaps 'transient-map
 "<escape>" #'transient-quit-all)

;; Eglot map
(general-define-key
 :states '(normal motion)
 :keymaps 'eglot-mode-map
 "]e" #'flymake-goto-next-error
 "[e" #'flymake-goto-prev-error)

(my/localleader
  :keymaps 'eglot-mode-map
  "f" #'eglot-format
  "a" #'eglot-code-actions
  "f" #'eglot-format
  "r" #'eglot-rename
  "t" #'eglot-find-typeDefinition
  "D" #'eglot-find-implementation
  "s" #'consult-eglot-symbols)

;; PDF-view map
(my/localleader
  :keymaps 'pdf-view-mode-map
  "m" #'pdf-view-midnight-minor-mode)

;; Git map
(general-define-key
 :keymaps 'my/git-map
 "SPC" #'my/git-transient
 "g" #'magit
 "]" #'diff-hl-next-hunk
 "[" #'diff-hl-previous-hunk
 "h" #'diff-hl-shot-hunk
 "s" #'diff-hl-stage-current-hunk
 "x" #'diff-hl-revert-hunk
 "t" #'git-timemachine-toggle
 "B" #'magit-blame-addition)

(general-define-key
 :states 'normal
 :keymaps 'git-timemachine-mode-map
 "q" #'git-timemachine-quit
 "C-n" #'git-timemachine-show-next-revision
 "C-p" #'git-timemachine-show-previous-revision)

;; Open map
(general-define-key
 :keymaps 'my/open-map
 ":" #'evil-command-window-ex
 "-" #'dired-jump
 ;; "T" #'my/vterm-here
 ;; "p" #'my/vterm-project
 "e" #'eshell
 "E" #'my/eshell-here
 "P" #'my/eshell-project
 ;; "t" #'vterm
 "f" #'make-frame
 "D" #'docker
 "x" #'my/open-html-file-with-xwidget
 "u" #'xwidget-webkit-browse-url
 "d" #'dirvish-side)

;; Quit map
(general-define-key
 :keymaps 'my/quit-map
 "q" '("Quit" . save-buffers-kill-terminal)
 "s" '("Save desktop quit Emacs" . my/save-desktop-quit)
 "l" '("Load last saved desktop" . my/load-desktop)
 "f" #'delete-frame)

;; File map
(general-define-key
 :keymaps 'my/file-map
 "s" #'save-buffer
 "r" #'consult-recent-file
 "t" #'consult-tramp
 "u" #'("Sudo open file" . my/sudo-find-file)
 "U" #'("Sudo this file" . my/sudo-this-file)
 "S" #'("Save as" . write-file)
 "R" #'rename-visited-file
 "f" #'find-file
 "D" (lambda () (interactive) (delete-file (buffer-file-name))))

;; Buffer map
(general-define-key
 :keymaps 'my/buffer-map
 "d" #'evil-delete-buffer
 "k" #'evil-delete-buffer
 "m" #'bookmark-set
 "M" #'bookmark-delete
 "N" #'evil-buffer-new
 "[" #'previous-buffer
 "]" #'next-buffer
 "R" #'rename-buffer
 "s" #'consult-buffer
 "i" #'ibuffer
 "l" #'evil-switch-to-windows-last-buffer
 "p" #'evil-switch-to-windows-last-buffer
 "SPC" #'display-buffer
 "s" #'switch-to-buffer
 "o" #'display-buffer
 "r" #'revert-buffer)

;; Tab map
(general-define-key
 :keymaps 'my/tab-map
 "n" #'tab-bar-new-tab
 "c" #'tab-bar-close-tab
 "k" #'tab-bar-close-tab
 "d" #'tab-bar-close-tab
 "C" #'tab-bar-close-group-tabs
 "o" #'tab-bar-close-other-tabs
 "]" #'tab-bar-switch-to-next-tab
 "[" #'tab-bar-switch-to-prev-tab
 "{" #'tab-bar-history-back
 "}" #'tab-bar-history-forward
 "b" #'tab-bar-move-window-to-tab
 "l" #'("Last tab" . tab-bar-switch-to-recent-tab)
 "p" #'("Last tab" . tab-bar-switch-to-recent-tab)
 "`" #'("Last tab" . tab-bar-switch-to-recent-tab)
 "L" #'tab-bar-move-tab
 "H" #'tab-bar-move-tab-backward
 "g" #'tab-bar-change-tab-group
 "r" #'tab-bar-rename-tab
 "1" '("Tab 1" . (lambda () (interactive) (tab-bar-select-tab 1)))
 "2" '("Tab 2" . (lambda () (interactive) (tab-bar-select-tab 2)))
 "3" '("Tab 3" . (lambda () (interactive) (tab-bar-select-tab 3)))
 "4" '("Tab 4" . (lambda () (interactive) (tab-bar-select-tab 4)))
 "5" '("Tab 5" . (lambda () (interactive) (tab-bar-select-tab 5)))
 "6" '("Tab 6" . (lambda () (interactive) (tab-bar-select-tab 6)))
 "7" '("Tab 7" . (lambda () (interactive) (tab-bar-select-tab 7)))
 "8" '("Tab 8" . (lambda () (interactive) (tab-bar-select-tab 8)))
 "9" '("Tab 9" . (lambda () (interactive) (tab-bar-select-tab 9)))
 "." #'tab-bar-switch-to-tab)

;; Insert map
(general-define-key
 :keymaps 'my/insert-map
 "s" #'yas-insert-snippet
 "c" #'insert-char)

;; Window map
(general-define-key
 :keymaps 'my/window-map
 "SPC" #'my/window-transient
 "w" #'ace-window
 "d" #'evil-window-delete
 "k" #'evil-window-delete
 "p" #'evil-window-mru
 "P" #'popper-toggle-type
 "W" #'evil-window-prev
 "s" #'evil-window-split
 "v" #'evil-window-vsplit
 "h" #'evil-window-left
 "j" #'evil-window-down
 "k" #'evil-window-up
 "l" #'evil-window-right
 "q" #'evil-quit
 "o" #'delete-other-windows
 "=" #'balance-windows
 "+" #'evil-window-increase-height
 "-" #'evil-window-decrease-height
 ":" #'evil-ex
 "<" #'evil-window-decrease-width
 ">" #'evil-window-increase-width
 "_" #'evil-window-set-height
 "|" #'evil-window-set-width
 "c" #'evil-window-delete
 "gd" #'xref-find-definitions-other-window
 "x" #'evil-window-exchange
 "r" #'evil-window-rotate-downwards
 "R" #'evil-window-rotate-upwards
 "H" #'evil-window-move-far-left
 "J" #'evil-window-move-very-bottom
 "K" #'evil-window-move-very-top
 "L" #'evil-window-move-far-right
 "m" #'delete-other-windows
 "u" #'winner-undo
 "=" #'balance-windows
 "C-r" #'winner-redo)

(general-define-key
 :states 'normal
 :keymaps 'dashboard-mode-map
 "RET" 'dashboard-return
 "r" #'dashboard-jump-to-recents
 "p" #'dashboard-jump-to-projects
 "b" #'dashboard-jump-to-bookmarks
 "l" #'my/load-desktop)

;; Popper
(general-define-key
 "C-`"   #'popper-toggle-latest
 "C-M-`" #'popper-cycle
 "C-M-~" #'popper-toggle-type)

;; Evil objects inner
(general-define-key
 :keymaps 'in
 "a" #'evil-inner-arg
 "a" #'evil-inner-buffer
 "q" #'my/evil-textobj-anyblock-inner-quote
 "g" #'my/evil-textobj-whole-buffer-txtobj
 "c" #'evilnc-inner-commenter
 "#" #'evilnc-inner-commenter
 "i" #'evil-indent-plus-i-indent
 "j" #'evil-indent-plus-i-indent-up-down
 "k" #'evil-indent-plus-i-indent-up)

;; Evil objects outer
(general-define-key
 :keymaps 'out
 "a" #'evil-outer-arg
 "q" #'my/evil-textobj-anyblock-outer-quote
 "g" #'my/evil-textobj-whole-buffer-txtobj
 "c" #'evilnc-outer-commenter
 "#" #'evilnc-outer-commenter
 "i" #'evil-indent-plus-i-indent
 "j" #'evil-indent-plus-i-indent-up-down
 "k" #'evil-indent-plus-i-indent-up)

;; Evil history
(general-define-key
 :keymaps '(evil-ex-completion-map evil-ex-search-keymap)
 "C-p" #'previous-complete-history-element
 "C-n" #'next-complete-history-element
 "C-k" #'kill-line
 "C-b" #'backward-char
 "C-f" #'forward-char
 "C-a" #'move-beginning-of-line)

;; Vertico map
(general-define-key
 :keymaps 'vertico-map
 "?" #'minibuffer-completion-help
 "M-RET" #'vertico-exit-input
 "C-SPC" #'my/embark-preview
 "<escape>" #'abort-minibuffers
 "?" #'minibuffer-completion-help
 "C-j" #'vertico-next
 "C-k" #'vertico-previous
 "C-M-n" #'vertico-next-group
 "C-M-j" #'vertico-next-group
 "C-M-k" #'vertico-previous-group
 "C-M-p" #'vertico-previous-group

 ;; Multiform toggles
 "<backspace>" #'vertico-directory-delete-char
 "C-w" #'vertico-directory-delete-word
 "C-<backspace>" #'vertico-directory-delete-word
 "RET" #'vertico-exit
 )


;; Corfu completion keymaps. Fixme: make it similar to other bindings
(general-def
  :keymaps 'completion-in-region-mode
  :definer 'minor-mode
  :states 'insert
  :predicate 'corfu-mode
  "<tab>" 'corfu-next
  "<backtab>" 'corfu-previous
  "C-j" 'corfu-next
  "C-n" 'corfu-next
  "C-k" 'corfu-previous
  "C-p" 'corfu-previous
  "C-l" 'corfu-complete
  "<escape>" 'corfu-quit
  "<return>" 'corfu-insert)

;; General main keys
(general-define-key
 "C-;" #'embark-act
 "C-B" #'embark-bindings
 "C-M-=" #'zoom-frm-in
 "C-M--" #'zoom-frm-out
 "C-+" #'zoom-frm-unzoom)

;; Occur and minibuffer editing
(general-define-key
 :keymaps '(minibuffer-local-map minibuffer-mode-map)
 "C-c C-e" #'embark-export)

(general-define-key
 :state 'normal
 :keymaps 'occur-edit-mode-map
 "C-c C-c" 'occur-cease-edit)

;; Programming
(my/localleader
  :states '(normal visual)
  :keymaps 'yas-minor-mode-map
  :infix "y"
  :which-key "YASNIPPET"
  "" '(nil :ignore t :which-key "Yasnippets")
  "y" #'yas-expand
  "n" #'yas-new-snippet
  "v" #'yas-visit-snippet-file
  "i" #'yas-insert-snippet)

;; Programming languages
(my/localleader
  :states '(normal visual)
  :keymaps '(python-mode-map python-ts-mode-map)
  :infix "m"
  "r" #'my/restart-python-repl
  "R" #'my/restart-python-repl-last-interpreter
  "s" #'run-python
  "p" #'poetry)

(my/localleader
  :keymaps '(python-mode-map python-ts-mode-map)
  :infix "t"
  "a" #'python-pytest
  "f" #'python-pytest-file-dwim
  "F" #'python-pytest-file
  "t" #'python-pytest-function-dwim
  "T" #'python-pytest-function
  "r" #'python-pytest-repeat
  "p" #'python-pytest-dispatch)

;; Org-mode (mostly taken from doomemacs)
(my/localleader
  :keymaps 'org-mode-map
  "#" #'org-update-statistics-cookies
  "'" #'org-edit-special
  "*" #'org-ctrl-c-star
  "+" #'org-ctrl-c-minus
  "," #'org-switchb
  "." #'org-goto
  "@" #'org-cite-insert
  "." #'consult-org-heading
  "/" #'consult-org-agenda
  "A" #'org-archive-subtree
  "e" #'org-export-dispatch
  "f" #'org-footnote-action
  "h" #'org-toggle-heading
  "i" #'org-toggle-item
  "I" #'org-id-get-create
  "k" #'org-babel-remove-result
  "n" #'org-store-link
  "o" #'org-set-property
  "q" #'org-set-tags-command
  "t" #'org-todo
  "T" #'org-todo-list
  "x" #'org-toggle-checkbox)

(my/localleader
  :keymaps 'org-mode-map
  :infix "a"
  "a" #'org-attach
  "d" #'org-attach-delete-one
  "D" #'org-attach-delete-all
  "n" #'org-attach-new
  "o" #'org-attach-open
  "O" #'org-attach-open-in-emacs
  "r" #'org-attach-reveal
  "R" #'org-attach-reveal-in-emacs
  "u" #'org-attach-url
  "s" #'org-attach-set-directory
  "S" #'org-attach-sync
  "c" #'org-download-screenshot
  "p" #'org-download-clipboard
  "P" #'org-download-yank)

(my/localleader
  :keymaps 'org-mode-map
  :infix "b"
  "-" #'org-table-insert-hline
  "a" #'org-table-align
  "b" #'org-table-blank-field
  "c" #'org-table-create-or-convert-from-region
  "e" #'org-table-edit-field
  "f" #'org-table-edit-formulas
  "h" #'org-table-field-info
  "s" #'org-table-sort-lines
  "r" #'org-table-recalculate
  "R" #'org-table-recalculate-buffer-tables)

(my/localleader
  :keymaps 'org-mode-map
  :infix "d"
  "c" #'org-table-delete-column
  "r" #'org-table-kill-row)

(my/localleader
  :keymaps 'org-mode-map
  :infix "i"
  "c" #'org-table-insert-column
  "h" #'org-table-insert-hline
  "r" #'org-table-insert-row
  "H" #'org-table-hline-and-move)

(my/localleader
  :keymaps 'org-mode-map
  :infix "t"
  "f" #'org-table-toggle-formula-debugger
  "o" #'org-table-toggle-coordinate-overlays)

(my/localleader
  :keymaps 'org-mode-map
  :infix "c"
  "c" #'org-clock-cancel
  "d" #'org-clock-mark-default-task
  "e" #'org-clock-modify-effort-estimate
  "E" #'org-set-effort
  "g" #'org-clock-goto
  "l" #'+org/toggle-last-clock
  "i" #'org-clock-in
  "I" #'org-clock-in-last
  "o" #'org-clock-out
  "r" #'org-resolve-clocks
  "R" #'org-clock-report
  "t" #'org-evaluate-time-range
  "=" #'org-clock-timestamps-up
  "-" #'org-clock-timestamps-down)

(my/localleader
  :keymaps 'org-mode-map
  :infix "d"
  "d" #'org-deadline
  "s" #'org-schedule
  "t" #'org-time-stamp
  "T" #'org-time-stamp-inactive)

(my/localleader
  :keymaps 'org-mode-map
  :infix "g"
  "g" #'org-goto
  "g" #'consult-org-heading
  "G" #'consult-org-agenda
  "c" #'org-clock-goto
  "i" #'org-id-goto
  "r" #'org-refile-goto-last-stored
  "x" #'org-capture-goto-last-stored)

(my/localleader
  :keymaps 'org-mode-map
  :infix "l"
  "c" #'org-cliplink
  "i" #'org-id-store-link
  "l" #'org-insert-link
  "L" #'org-insert-all-links
  "s" #'org-store-link
  "S" #'org-insert-last-stored-link
  "t" #'org-toggle-link-display)

(my/localleader
  :keymaps 'org-mode-map
  :infix "P"
  "a" #'org-publish-all
  "f" #'org-publish-current-file
  "p" #'org-publish
  "P" #'org-publish-current-project
  "s" #'org-publish-sitemap)

(my/localleader
  :keymaps 'org-mode-map
  :infix "r"
  "r" #'org-refile
  "R" #'org-refile-reverse)

(my/localleader
  :keymaps 'org-mode-map
  :infix "s"
  "a" #'org-toggle-archive-tag
  "b" #'org-tree-to-indirect-buffer
  "c" #'org-clone-subtree-with-time-shift
  "d" #'org-cut-subtree
  "h" #'org-promote-subtree
  "j" #'org-move-subtree-down
  "k" #'org-move-subtree-up
  "l" #'org-demote-subtree
  "n" #'org-narrow-to-subtree
  "r" #'org-refile
  "s" #'org-sparse-tree
  "A" #'org-archive-subtree
  "N" #'widen
  "S" #'org-sort)

(my/localleader
  :keymaps 'org-mode-map
  :infix "p"
  "d" #'org-priority-down
  "p" #'org-priority
  "u" #'org-priority-up)

(general-define-key
 :states 'normal
 :keymaps 'org-journal-mode-map
 "]f"  #'org-journal-next-entry
 "[f"  #'org-journal-previous-entry
 "C-n" #'org-journal-next-entry
 "C-p" #'org-journal-previous-entry)

(general-define-key
 :keymaps 'org-journal-search-mode-map
 "C-n" #'org-journal-search-next
 "C-p" #'org-journal-search-previous)

(my/localleader
  :keymaps 'org-journal-mode-map
  :infix "j"
  "c" #'org-journal-new-entry
  "d" #'org-journal-new-date-entry
  "n" #'org-journal-next-entry
  "p" #'org-journal-previous-entry
  "j" #'org-journal-new-entry
  "J" #'org-journal-new-scheduled-entry
  "s" #'org-journal-search-forever)

(my/localleader
  :keymaps 'org-journal-mode-map
  :infix "s"
  "s" #'org-journal-search
  "f" #'org-journal-search-forever
  "F" #'org-journal-search-future
  "w" #'org-journal-search-calendar-week
  "m" #'org-journal-search-calendar-month
  "y" #'org-journal-search-calendar-year)

(my/localleader
  :keymaps 'org-journal-search-mode-map
  "n" #'org-journal-search-next
  "p" #'org-journal-search-prev)

;; Transients
(transient-define-prefix my/toggle-transient ()
  [["Looks"
    ("b" "Big font" my/big-mode)
    ("R" "rainbow-delimiters-mode" rainbow-delimiters-mode :transient t)
    ("c" "my/highlight-long-lines" my/highlight-long-lines :transient t)
    ("C" "display-fill-column-indicator-mode" display-fill-column-indicator-mode :transient t)
    ("d" "my/dark-mode" my/dark-mode)
    ("F" "fringe-mod"  fringe-mode)
    ("L" "ligature-mode" ligature-mode)
    ("i" "indent-guide-mode" indent-guide-mode)
    ("I" "lowlight-inactive-windows" my/lowlight-inactive-buffer-mode)
    ("l" "my/line-number-transient" my/line-number-transient)
    ("B" "breadcrumb-local-mode" breadcrumb-local-mode)
    ("g" "diff-hl-mode" diff-hl-mode)
    ("w" "Wrap line" visual-line-mode)
    ("z" "Zen mode" my/zen-mode)]

   ["Editing"
    ("s" "flyspell-mode " flyspell-mode)
    ("f" "flycheck-mode " flycheck-mode)
    ("r" "read-only-mode " read-only-mode)
    ("h" "evil-ex-nohighlight" evil-ex-nohighlight)]
   ["Misc"
    ("q" "Quit" transient-quit-one)]])

(transient-define-prefix my/window-transient ()
  [["Size"
    ("-" "shrink horizontally" shrink-window-horizontally :transient t)
    ("+" "enlarge horizontally" enlarge-window-horizontally :transient t)
    ("=" "balance window height" balance-windows :transient t)
    ("m" "maximize current window" maximize-window :transient t)
    ("M" "minimize current window" minimize-window :transient t)]
   ["Split"
    ("s" "split horizontally" split-window-below :transient t)
    ("v" "split vertically" split-window-right :transient t)
    ("d" "delete current window" delete-window :transient t)
    ("D" "delete-other-windows" delete-other-windows :transient t)]
   ["Move"
    ("z" "ace window" ace-window :transient t)
    ("h" "← window" windmove-left :transient t)
    ("j" "↓ window" windmove-down :transient t)
    ("k" "↑ window" windmove-up :transient t)
    ("l" "→ window" windmove-right :transient t)]
   ["Misc"
    ("q" "Quit" transient-quit-one)]])

(transient-define-prefix my/git-transient ()
  [["Main"
    ("n" "next hunk" diff-hl-next-hunk :transient t)
    ("p" "previous hunk" diff-hl-previous-hunk :transient t)
    ("h" "first hunk" (lambda () (interactive) (progn (goto-char (point-min)) (diff-hl-next-hunk 1))) :transient t)
    ("l" "last hunk" (lambda ()  (interactive) (progn (goto-char (point-min)) (diff-hl-previous-hunk 1))) :transient t)
    ("<SPC>" "popup hunk" diff-hl-show-hunk :transient t)
    ("s" "stage hunk" diff-hl-stage-current-hunk :transient t)
    ("r" "revert hunk" diff-hl-revert-hunk)]
   ["Misc"
    ("q" "Quit" transient-quit-one)]])

(transient-define-prefix my/line-number-transient ()
  [["Main"
    ("r" "relative" (lambda() (interactive) (setq display-line-numbers 'relative)))
    ("n" "none" (lambda() (interactive) (display-line-numbers-mode 0)))
    ("a" "absolute" (lambda() (interactive) (setq display-line-numbers t)))]
   ["Misc"
    ("q" "Quit" transient-quit-one)]])

(transient-define-prefix my/debug-transient ()
  [["Stepping"
    ("n" "Next" dape-next :transient t)
    ("i" "Step in" dape-step-in :transient t)
    ("o" "Step out" dape-step-out :transient t)
    ("c" "Continue" dape-continue :transient t)
    ("r" "Restart" dape-restart :transient t)]
   ["Breakpoints"
    ("b" "Toggle" dape-breakpoint-toggle :transient t)
    ("d" "Delete" dape-remove-breakpoint-at-point :transient t)
    ("D" "Delete all" dape-breakpoint-remove-all :transient t)
    ("l" "Log" dape-breakpoint-log :transient t)]
   ["Debugger"
    ("q" "Quit" dape-quit)
    ("s" "Start" dape)
    ("k" "Kill" dape-kill)]]
  )

;; Comand to open a terminal in the current directory! Nice
(defun run-kitty-terminal-here ()
  "Open a new Kitty terminal in the current directory."
  (interactive)
  (let ((cmd (concat "kitty --directory "
                     (shell-quote-argument (file-name-directory (or load-file-name buffer-file-name)))
                     " > /dev/null 2>&1 & disown")))
    (message "Running: %s" cmd)  ;; Log the command for debugging
    (shell-command cmd nil nil)))

(general-define-key
 :prefix "SPC"
 :states '(normal visual motion) ;; Using Evil mode
 "o t" 'run-kitty-terminal-here)

;; (global-set-key (kbd "SPC o t") 'run-kitty-terminal-here)
