(provide 'my-programming)

;; Enable if you need lsp
(defun efs/lsp-mode-setup ()
  (setq lsp-headerline-breadcrumb-segments '(path-up-to-project file symbols))
  (lsp-headerline-breadcrumb-mode))

(use-package lsp-mode
  :hook ((c++-mode c-mode arduino-mode python-mode latex-mode js-mode) . efs/lsp-mode-setup)
  :commands lsp lsp-deferred
  :custom
  ;; General settigs
  (lsp-auto-guess-root t)
  (lsp-modeline-diagnostics-enable t)
  (lsp-modeline-diagnostics-enable t)
  (lsp-headerline-breadcrumb-enable t)
  (lsp-completion-show-detail t)
  (lsp-completion-show-kind t)
  (lsp-signature-render-documentation t)
  ;; Performance tuning
  (lsp-log-io nil)
  (lsp-print-performance nil)
  (lsp-enable-file-watchers nil)
  (lsp-idle-delay 0.500)
  (gc-cons-threshold 100000000)
  (read-process-output-max 1048576))

;; Enhancements for lsp-mode
;; (use-package lsp-ui
;;   :hook (lsp-mode . lsp-ui-mode)
;;   :custom
;;   (lsp-ui-doc-position 'bottom))
(use-package lsp-ui
  :commands lsp-ui-mode
  :config
  (setq lsp-ui-flycheck-enable t)
  (setq lsp-ui-sideline-enable t)
  (setq lsp-ui-sideline-show-hover nil)
  (setq lsp-ui-imenu-enable t)
  (setq lsp-ui-peek-fontify 'on-demand)
  (setq lsp-ui-peek-enable t)
  (setq lsp-ui-doc-enable t)
  (setq lsp-ui-doc-max-height 24)
  (setq lsp-ui-doc-max-width 56)
  (setq lsp-ui-doc-position 'bottom)
  (setq lsp-ui-doc-border "#323445")
  (setq lsp-ui-doc-header t)
  (setq lsp-ui-doc-include-signature t)
  ;; (setq lsp-ui-doc-border (face-foreground 'default))
  (setq lsp-ui-sideline-show-code-actions t)
  (setq lsp-ui-sideline-delay 0.05))

(use-package dap-mode
  ;; Uncomment the config below if you want all UI panes to be hidden by default!
  ;; :custom
  ;; (lsp-enable-dap-auto-configure nil)
  ;; :config
  ;; (dap-ui-mode 1)
  :commands dap-debug
  :config
  ;; Set up Node debugging
  (require 'dap-node)
  (dap-node-setup) ;; Automatically installs Node debug adapter if needed

  ;; Bind `C-c l d` to `dap-hydra` for easy access
  (general-define-key
    :keymaps 'lsp-mode-map
    :prefix lsp-keymap-prefix
    "d" '(dap-hydra t :wk "debugger")))

;; setting up python
(use-package lsp-pyright
  :hook (python-mode . (lambda () (require 'lsp-pyright)))
  :init (when (executable-find "python3")
          (setq lsp-pyright-python-executable-cmd "python3")))
;; (use-package python-mode
;;   :ensure t
;;   :hook (python-mode . lsp-deferred)
;;   :custom
;;   (dap-python-debugger 'debugpy)
;;   :config
;;   (require 'dap-python))

(use-package lsp-latex
  :hook (latex-mode . (lambda () (require 'lsp-texlab))))

(with-eval-after-load "tex-mode"
 (add-hook 'tex-mode-hook #'lsp)
 (add-hook 'latex-mode-hook #'lsp))

;; For YaTeX
(with-eval-after-load "yatex"
 (add-hook 'yatex-mode-hook #'lsp))

;; For bibtex
(with-eval-after-load "bibtex"
 (add-hook 'bibtex-mode-hook #'lsp))

;; company mode
;; (use-package company
;;   :after lsp-mode
;;   :hook (lsp-mode . company-mode)
;;   :bind (:map company-active-map
;;          ("<tab>" . company-complete-selection))
;;         (:map lsp-mode-map
;;          ("<tab>" . company-indent-or-complete-common))
;;   :custom
;;   (company-minimum-prefix-length 1)
;;   (company-idle-delay 0.0))

;; (use-package company-box
;;   :hook (company-mode . company-box-mode))

;;(use-package format-all)

;; AucTeX configuration
(use-package tex
  :straight auctex
  :mode ("\\.tex\\'" . LaTeX-mode)
  :config
  (setq TeX-engine 'luatex)
  ;;      LaTeX-command "lualatex -shell-escape")
  (setq TeX-view-program-selection (quote ((output-pdf "Zathura") (output-dvi "xdvi")))))

(setq TeX-engine (quote luatex))
(setq predictive-latex-electric-environments 1)

;; define environments we don't want to autofill:
(defvar unfillable-envs '("figure" "tikzpicture" "table"))
;; add more environments if you like, eg:
;; (defvar unfillable-envs '("figure" "tikzpicture"))

;; create a function to check the environment first, then fill:
(defun my-filtered-fill ()
  (unless (member (LaTeX-current-environment) unfillable-envs)
    (do-auto-fill)))

;; setup LaTeX/AucTex to use auto-fill-mode, but with our fill-function:
(defun my-tex-auto-fill ()
  (auto-fill-mode)
  (setq auto-fill-function 'my-filtered-fill))

(add-hook 'LaTeX-mode-hook 'my-tex-auto-fill)
(add-hook 'LaTeX-mode-hook 'lsp)

;; Javascript mode
(defun my/setup-js-mode ()
  (require 'dap-firefox)
  (require 'dap-chrome)
  (setq tab-width 2)
  (when (require 'lsp-javascript nil t)
    (lsp)))
(use-package js2-mode
  :after (lsp-mode dap-mode)
  :mode "\\.js\\'"
  :hook ((js2-mode . my/setup-js-mode)))
(use-package typescript-mode
  :after (lsp-mode dap-mode)
  :mode ("\\.ts\\'" "\\.tsx\\'")
  :hook ((typescript-mode . my/setup-js-mode)))

;; c++ mode?
(setq package-selected-packages '(lsp-mode yasnippet lsp-treemacs
    projectile hydra flycheck company avy which-key dap-mode))

(when (cl-find-if-not #'package-installed-p package-selected-packages)
  (package-refresh-contents)
  (mapc #'package-install package-selected-packages))

;; sample `helm' configuration use https://github.com/emacs-helm/helm/ for details
;; (helm-mode)
;; (require 'helm-xref)
;; (define-key global-map [remap find-file] #'helm-find-files)
;; (define-key global-map [remap execute-extended-command] #'helm-M-x)
;; (define-key global-map [remap switch-to-buffer] #'helm-mini)

(which-key-mode)
(add-hook 'c-mode-hook 'lsp)
(add-hook 'c++-mode-hook 'lsp)

(setq gc-cons-threshold (* 100 1024 1024)
      read-process-output-max (* 1024 1024)
      treemacs-space-between-root-nodes nil
      company-idle-delay 0.0
      company-minimum-prefix-length 1
      lsp-idle-delay 0.1)  ;; clangd is fast

(with-eval-after-load 'lsp-mode
  (add-hook 'lsp-mode-hook #'lsp-enable-which-key-integration)
  (require 'dap-cpptools)
  (yas-global-mode))
;; (add-to-list 'auto-mode-alist '("\\.ino\\'" . c-mode))

;; (defconst my/ccls-cache-directory (eval-when-compile (expand-file-name ".ccls-cache" user-emacs-directory)))

;; (defconst my/clang (executable-find "clang"))
;; (if my/clang
;;     (defconst my/clang-resource-dir
;;       (string-trim (shell-command-to-string (concat my/clang " -print-resource-dir"))))
;;   (defconst my/clang-resource-dir nil))

;; (use-package ccls
;;   :init (setq ccls-executable (executable-find "ccls")
;;               ccls-initialization-options `(:clang (:resourceDir ,my/clang-resource-dir)
;;                                             :cache (:directory ,my/ccls-cache-directory)
;;                                             :index (:comments 2)))
;;   :config (require 'dap-lldb)
;;   :hook (((c-mode c++-mode arduino-mode) . yas-minor-mode)
;;          ((c-mode c++-mode arduino-mode) . flyspell-prog-mode)
;;          ((c-mode c++-mode arduino-mode objc-mode) . (lambda ()
;;                                           (require 'ccls)
;;                                           (lsp)))))

;; (use-package company-c-headers
;;   :config
;;   (push 'company-c-headers company-backends))

(use-package cmake-mode
  :ensure t
  :mode ("CMakeLists\\.txt\\'" "\\.cmake\\'")
  :hook (cmake-mode . lsp-deferred))

(use-package cmake-font-lock
  :ensure t
  :after cmake-mode
  :config (cmake-font-lock-activate))

;; Yasnippet
(use-package yasnippet
  :ensure t
  :config
  (yas-global-mode 1)
  (setq yas-snippet-dirs (list (concat user-emacs-directory "snippets")))
  :init
  (add-hook 'prog-mode-hook #'yas-minor-mode)
  (add-hook 'text-mode-hook #'yas-minor-mode)
  (add-hook 'yasnippet-mode-hook #'yas-reload-all))

(use-package doom-snippets
  :after yasnippet
  :straight (doom-snippets :type git :host github :repo "doomemacs/snippets" :files ("*.el" "*")))

(use-package realgud
  :ensure t)

(use-package realgud-trepan-ni
  :ensure t)

(setq compilation-scroll-output t)
;; Color escape characters for compilation
(add-hook 'compilation-filter-hook 'ansi-color-compilation-filter)

(add-hook 'python-mode-hook #'lsp)

(set-variable 'python-indent-guess-indent-offset nil)
(setq python-indent-guess-indent-offset-verbose nil)

;; Highlight indent
(use-package highlight-indent-guides)

(add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
(setq highlight-indent-guides-method 'character)
;; (setq highlight-indent-guides-odd-face "darkgray")
(setq highlight-indent-guides-odd-face "white")

;; Flyspell
(global-set-key
 [f3]
 (lambda ()
   (interactive)
   (ispell-change-dictionary "ngerman")))
(global-set-key
 [f4]
 (lambda ()
   (interactive)
   (ispell-change-dictionary "english")))
