(provide 'my-shell)

(use-package vterm
  :ensure t)

(defun my/vterm-here ()
  "Open a terminal buffer in the current window at cwd"
  (interactive)
  (vterm (concat "*vterm-" (string (random 255)) "*")))

(defun my/vterm-project ()
  "Open a terminal buffer in the current window at project root"
  (interactive)
  (let ((default-directory (project-root (project-current t))))
    (vterm "*vterm*")))

(use-package eshell-git-prompt
  :init
  (eshell-git-prompt-use-theme 'powerline))

(defun my/eshell-here ()
  "Open an eshell buffer in the current window at cwd"
  (interactive)
  (let ((current-prefix-arg (random 255))) (call-interactively #'eshell)))

(defun my/eshell-project ()
  "Open an eshell buffer in the current window at project root"
  (interactive)
  (let ((default-directory (project-root (project-current t))))
    (eshell)))

;; hide line numbers and don't truncate lines
(dolist (mode '(term-mode-hook
                dired-mode-hook
                vterm-mode-hook
                shell-mode-hook
                eshell-mode-hook
                comint-mode-hook
                dape-repl-mode-hook
                inferior-python-mode-hook))
  (add-hook mode (lambda ()
                   (display-line-numbers-mode 0)
                   (hide-mode-line-mode +1)
                   (visual-line-mode +1))))

(use-package capf-autosuggest
  :ensure t
  :hook
  ('comint-mode-hook #'capf-autosuggest-mode)
  ('eshell-mode-hook #'capf-autosuggest-mode))

(use-package fish-mode)
