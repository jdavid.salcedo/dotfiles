(provide 'my-core)

;; Disable startup message
(setq inhibit-startup-message t)

;; Set font size
(set-face-attribute 'default nil :height 120)

;; Enable line numbers
(global-display-line-numbers-mode t)

;; Increase the undo limit
(setq undo-limit 100000)

;; Increase garbage collection threshold during startup to improve performance
(setq gc-cons-threshold most-positive-fixnum)

;; Decrease garbage collection threshold after startup for better interactive performance
(add-hook 'emacs-startup-hook
          (lambda ()
            (setq gc-cons-threshold (* 8 1024 1024))))

;; Disable toolbars
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(set-fringe-mode 10)
(column-number-mode t)

;; Set default tab width to 4 spaces
(setq-default tab-width 4)

;; Enable electric-pair mode for automatic pairing of brackets and quotes
(electric-pair-mode 1)

;; Show matching parentheses
(show-paren-mode 1)

;; Highlight current line
(global-hl-line-mode 1)

;; Enable clipboard integration with system
(setq select-enable-clipboard t)

;; Configure caches
(defvar user-cache-directory (concat user-emacs-directory "cache/"))
(defvar user-data-directory (concat user-emacs-directory "data/"))

(setq recentf-save-file (concat user-cache-directory "recentf")
      eshell-history-file-name (concat user-cache-directory "eshell-history")
      savehist-file (concat user-cache-directory "savehist-history")
      request-storage-directory (concat user-cache-directory "request")
      transient-history-file (concat user-cache-directory "transient-history.el")
      transient-values-file (concat user-cache-directory "transient-values.el")
      transient-levels-file (concat user-cache-directory "transient-levels.el")
      bookmark-default-file (concat user-cache-directory "bookmarks")
      tramp-persistency-file-name (concat user-cache-directory "tramp")
      straight-base-dir user-cache-directory)
(recentf-mode 1)

;; (when (featurep 'comp)
;;   (startup-redirect-eln-cache (concat user-cache-directory "eln-cache")))

;; (let ((backup-dir (concat user-cache-directory "backups/"))
;;       (autosave-dir (concat user-cache-directory "auto-saves/")))
;;   ;; create directories if needed
;;   (dolist (path (list backup-dir autosave-dir))
;;     (unless (file-directory-p path)
;;       (make-directory path t)))

;;   (setq backup-directory-alist `((".*" . ,backup-dir))
;;         auto-save-file-name-transforms `((".*" ,autosave-dir t))
;;         create-lockfiles nil
;;         auto-save-list-file-prefix (concat autosave-dir "saves-")
;;         tramp-backup-directory-alist `((".*" . ,backup-dir))
;;         tramp-auto-save-directory autosave-dir))

(setq abbrev-file-name (concat user-data-directory "abbrev"))
(setq custom-file (concat user-emacs-directory "custom.el"))

;; Set default major mode for new buffers to text-mode
(setq-default major-mode 'text-mode)

;; Customize indentation
(setq-default indent-tabs-mode nil)    ; Use spaces instead of tabs for indentation
(setq-default c-basic-offset 4)        ; Set C/C++ indentation level to 4 spaces
(setq js-indent-level 2)               ; Set JavaScript indentation level to 2 spaces
(setq web-mode-markup-indent-offset 2) ; Set HTML indentation level to 2 spaces in web-mode
