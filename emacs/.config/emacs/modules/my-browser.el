(provide 'my-browser)
;; taken from https://git.sr.ht/~northyear/dotemacs
(defun my/open-html-file-with-xwidget (url new-session)
  "open local file (html) using xwidget,
prefix with C-u to open the url with a new xwidget session"
  (interactive "G\nP")
  (setq url (concat "file://"
                    (expand-file-name url)))
  (xwidget-webkit-browse-url url new-session))

(defun my/open-buffer-with-xwidget (new-session)
  "open local file (html) using xwidget,
prefix with C-u to open the url with a new xwidget session"
  (interactive "P")
  (setq url (concat "file://"
                    (buffer-file-name)))
  (xwidget-webkit-browse-url url new-session))

;; preview with dirvish
(when (featurep 'xwidget-internal)
  (dirvish-define-preview html (file ext)
    "Use xwidgets for HTML file previews"
    (setq url (concat "file://"
                      file))
    (when (equal ext "html")
      (progn
        (xwidget-webkit-browse-url url)
        (evil-window-mru))))

  (add-to-list 'dirvish-preview-dispatchers 'html))

(add-hook 'xwidget-webkit-mode-hook
          (lambda() (display-line-numbers-mode 0)
            (hide-mode-line-mode +1)))
