(provide 'my-project)

(use-package project
    :config
    (add-to-list 'project-switch-commands
                 '(project-dired "Dired at root"))
    (setq project-list-file (concat user-cache-directory "projects"))

    (remove-hook 'project-switch-commands '(project-vc-dir "VC-Dir")))
