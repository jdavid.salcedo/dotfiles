(provide 'my-editing)

(use-package evil-surround)
(use-package evil-traces)
(use-package evil-exchange)
(use-package evil-visualstar)
(use-package evil-replace-with-register)
(use-package evil-args)
(use-package evil-lion)
(use-package evil-indent-plus)
(use-package evil-numbers)
(use-package expand-region)
(use-package evil-matchit)
(use-package evil-textobj-anyblock)

(use-package evil
  :init
  (setq evil-want-C-i-jump t
        evil-want-C-u-scroll nil
        evil-want-Y-yank-to-eol t
        evil-want-C-u-scroll t
        evil-want-C-h-delete t
        evil-want-C-w-delete t
        evil-symbol-word-search t
        evil-want-keybinding nil
        evil-want-abbrev-expand-on-insert-exit t
        evil-visual-update-x-selection-p nil
        evil-mode-line-format nil
        evil-ex-search-vim-style-regexp t
        evil-ex-visual-char-range t
        evil-mode-line-format nil
        evil-normal-state-cursor 'box
        evil-insert-state-cursor 'bar
        evil-emacs-state-cursor 'box
        evil-visual-state-cursor 'hollow
        evil-ex-interactive-search-highlight 'selected-window
        evil-kbd-macro-suppress-motion-error t
        evil-undo-system 'undo-tree
        evil-ex-hl-update-delay 0.1)
  :config
  ;; Remove C-l from evil-repeat commands
  (evil-declare-abort-repeat 'recenter-top-bottom)
)

(use-package evil-goggles
  :init
  (setq evil-goggles-duration 0.5
        evil-goggles-pulse nil
        evil-goggles-enable-delete nil
        evil-goggles-enable-change nil))

(use-package evil-org
  :hook (org-mode . evil-org-mode)

  :config
  (evil-org-set-key-theme)
  (add-hook 'org-capture-mode-hook #'evil-insert-state))

(use-package evil-escape
  :init
  (setq evil-escape-key-sequence "jk"))

(use-package evil-embrace
  :commands (embrace-add-pair embrace-add-pair-regexp)
  :hook ((LaTeX-mode . embrace-LaTeX-mode-hook)
         (org-mode . embrace-org-mode-hook)
         (emacs-lisp-mode . embrace-emacs-lisp-mode-hook)))

(use-package evil-nerd-commenter
  :commands (evilnc-comment-operator
             evilnc-inner-commenter
             evilnc-outer-commenter)
  :config
  (general-define-key
   [remap comment-line] #'evilnc-comment-or-uncomment-lines))

(use-package evil-snipe
  :init
  (setq evil-snipe-smart-case t
        evil-snipe-scope 'buffer
        evil-snipe-use-vim-sneak-bindings t
        evil-snipe-repeat-keys nil
        evil-snipe-char-fold t))

(use-package evil-anzu
  :demand t
  :after anzu)

(use-package better-jumper)


(use-package evil-collection
  :after evil
  :ensure t
  :config
  (evil-collection-init)
  :init
  (setq evil-collection-key-blacklist '("SPC" "gj" "gk" "gt" "gT")
        evil-collection-mode-list
        '(arc-mode bm bookmark consult comint compile eldoc daemons
                   debug diff-hl diff-mode dired dired-sidebar
                   docker doc-view elisp-refs embark eldoc eshell
                   eww elfeed flymake grep help helpful ibuffer
                   imenu macrostep magit-sections magit magic-todos
                   man markdown-mode mu4e notmuch org org-roam
                   osx-dictionary pdf python replace rg ripgrep
                   tab-bar term vertico vterm wdired wgrep which-key
                   xref xwidget flymake flycheck)
        evil-collection-want-unimpaired-p nil))

(use-package evil-mc
  :init
  (global-evil-mc-mode 1)
  :config
  ;; Clear default keybinds/behavior
  (evil-define-key '(normal visual) 'global
    (kbd "C-<down-mouse-2>") nil)
  (evil-define-key '(normal visual) evil-mc-key-map
    (kbd "C-p") nil
    (kbd "C-n") nil
    (kbd "M-p") nil
    (kbd "M-n") nil)
  (setq evil-mc-undo-cursors-on-keyboard-quit t))

(use-package edit-indirect)

;; Define my own evil text objects
(evil-define-text-object my/evil-textobj-anyblock-inner-quote
  (count &optional beg end type)
  "Select the closest outer quote."
  (require 'evil-textobj-anyblock)
  (let ((evil-textobj-anyblock-blocks
         '(("'" . "'")
           ("\"" . "\"")
           ("`" . "'")
           ("“" . "”"))))
    (evil-textobj-anyblock--make-textobj beg end type count nil)))

(evil-define-text-object my/evil-textobj-anyblock-outer-quote
  (count &optional beg end type)
  "Select the closest outer quote."
  (require 'evil-textobj-anyblock)
  (let ((evil-textobj-anyblock-blocks
         '(("'" . "'")
           ("\"" . "\"")
           ("`" . "'")
           ("“" . "”"))))
    (evil-textobj-anyblock--make-textobj beg end type count t)))

(evil-define-text-object my/evil-textobj-whole-buffer-txtobj (count &optional _beg _end type)
  "Text object to select the whole buffer."
  (evil-range (point-min) (point-max) type))

(use-package yafolding
  :ensure t
  :init
  (add-hook 'prog-mode-hook 'yafolding-mode))

;; delete trailing whitespace on save
(add-hook 'before-save-hook
          (lambda ()
            (unless (eq major-mode 'fundamental-mode)
              (delete-trailing-whitespace))))

(evil-mode 1)
(evil-select-search-module 'evil-search-module 'evil-search)
(evil-goggles-mode)
(evil-escape-mode)
(global-evil-surround-mode)
(evil-embrace-enable-evil-surround-integration)
(evil-traces-mode)
(evil-exchange-install)
(evil-snipe-mode)
(evil-snipe-override-mode)
(global-evil-visualstar-mode)
(better-jumper-mode)
(global-anzu-mode)
(global-evil-matchit-mode)
(evil-collection-init)
