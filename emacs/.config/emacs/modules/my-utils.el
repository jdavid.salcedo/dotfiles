(provide 'my-utils)

;; bootstrap straight.el, copied from
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-cache-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(use-package straight
  :custom
  (straight-use-package 'use-package)
  (straight-use-package-by-default t)
  :config
  ;; packages that I don't want to be installed
  (add-to-list 'straight-built-in-pseudo-packages 'company)
  (add-to-list 'straight-built-in-pseudo-packages 'projectile))

(use-package undo-tree
  :init
  (global-undo-tree-mode)
  :custom (undo-tree-history-directory-alist `(("." . ,(concat user-cache-directory "undo-tree-hist/"))))
  :config
  (setq undo-tree-visualizer-diff t
        undo-tree-visualizer-timestamps t
        undo-tree-enable-undo-in-region t
        undo-limit 800000
        undo-strong-limit 12000000
        undo-outer-limit 128000000
        undo-tree-auto-save-history t))

(use-package consult
  :hook (completion-list-mode . consult-preview-at-point-mode)
  :init
  (setq register-preview-delay 0.5
        register-preview-function #'consult-register-format)
  (advice-add #'register-preview :override #'consult-register-window)
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)
  :config
  (consult-customize
   consult-theme :preview-key '(:debounce 0.2 any)
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-bookmark consult--source-file-register
   consult--source-recent-file consult--source-project-recent-file
   :preview-key '(:debounce 0.4 any))
  (setq consult-narrow-key "<")) ;; "C-+"

(use-package marginalia
  :bind (:map minibuffer-local-map
              ("M-A" . marginalia-cycle))
  :init
  (marginalia-mode))

(use-package popper
  :ensure t
  :init
  (setq popper-reference-buffers
        '(help-mode
          compilation-mode
          eldoc-mode
          vterm-mode
          eshell-mode
          shell-mode
          term-mode
          "\\*Messages\\*"
          "\\*Async Shell Command\\*"
          "Output\\*$"
          "^\\*eshell*\\*$"
          "^\\*shell*\\*$"
          "^\\*term*\\*$"
          "^\\*vterm*\\*$"
          "^\\*warnings*\\*$"
          "^\\*eldoc*\\*$"
          "^\\*eldoc .+\\*$"))

  (popper-mode +1)
  (popper-echo-mode +1))

(use-package magit
  :ensure t
  :config
  (setq magit-section-initial-visibility-alist '((untracked . hide) (stashes . hide))))

(use-package diff-hl
  :ensure t
  :config
  (add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)
  (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)
  :init
  (global-diff-hl-mode 1))

(use-package vertico
  :demand t                             ; Otherwise won't get loaded immediately
  :straight (vertico :files (:defaults "extensions/*") ; Special recipe to load extensions conveniently
                     :includes (vertico-indexed
                                vertico-mouse
                                vertico-buffer
                                vertico-repeat
                                vertico-directory
                                vertico-multiform))

  :hook ((rfn-eshadow-update-overlay . vertico-directory-tidy) ; Clean up file path when typing
         (minibuffer-setup . vertico-repeat-save)) ; Make sure vertico state is saved

  :custom
  (vertico-count 13)
  (vertico-resize nil)
  (vertico-cycle nil)
  ;; Extensions
  (vertico-grid-separator "       ")
  (vertico-grid-lookahead 50)
  (vertico-buffer-display-action '(display-buffer-reuse-window))

  :config
  (vertico-mode)
  ;; Extensions
  (vertico-multiform-mode))

(use-package corfu
  ;; :init
  ;; (global-corfu-mode) ;; this messes up my stock autocomplete system
  :hook ((prog-mode . corfu-mode)
         (shell-mode . corfu-mode)
         (vterm-mode . corfu-mode)
         (eshell-mode . corfu-mode))
  :custom
  (setq completion-cycle-threshold 3)
  (setq tab-always-indent 'complete))

;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
  :init
  (savehist-mode)
  :config
  (setq savehist-additional-variables '(vertico-repeat-history
                                        evil-jumps-history
                                        kill-ring
                                        register-alist
                                        mark-ring
                                        global-mark-ring
                                        search-ring
                                        regexp-search-ring
                                        buffer-undo-list))
  (setq completion-in-region-function
        (lambda (&rest args)
          (apply (if vertico-mode
                     #'consult-completion-in-region
                   #'completion--in-region)
                 args))))

(use-package orderless
  :custom
  (completion-styles '(basic substring orderless partial-completion flex))
  (completion-category-defaults nil)

  (orderless-component-separator 'orderless-escapable-split-on-space)
  (orderless-matching-styles
   '(orderless-literal
     orderless-prefixes
     orderless-initialism
     orderless-regexp)))

(use-package embark
  :ensure t
  :init
  (setq prefix-help-command #'embark-prefix-help-command)
  :config
  (defun my/embark-preview ()
    "Previews candidate in vertico buffer, unless it's a consult command (taken from Doom Emacs)"
    (interactive)
    (unless (bound-and-true-p consult--preview-function)
      (save-selected-window
        (let (embark-quit-after-action)
          (embark-dwim))))
    ))

(use-package embark-consult
  :ensure t
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(use-package yasnippet
  :init
  (setq yas-verbosity 2))

(use-package rg
  :config
  (rg-define-search my/project-todos
    :query "(TODO|FIXME)"
    :format regexp
    :dir project
    :files current))

(use-package wgrep
  :ensure t)

(use-package git-timemachine)

(use-package dirvish
  :defer t
  :init
  (dirvish-override-dired-mode)
  :hook (dired-mode . dired-omit-mode)
  :custom
  (dirvish-quick-access-entries
   '(("h" "~/"                          "Home")
     ("d" "~/Downloads/"                "Downloads")
     ("m" "/mnt/"                       "Drives")
     ("N" "~/Nextcloud"                 "Nextcloud")
     ("p" "~/Projects"                  "Projects")
     ("P" "~/Nextcloud/Projects"        "Projects")
     ("t" "~/.local/share/Trash/files/" "TrashCan")))
  :config
  (setq dirvish-cache-dir (concat user-cache-directory "dirvish/")
        dirvish-hide-details nil
        dirvish-attributes '(git-msg nerd-icons)
        dirvish-reuse-session t
        dirvish-side-follow-mode t
        dirvish-subtree-listing-switches "-ahl -v --group-directories-first --almost-all"
        dirvish-attributes '(vc-state subtree-state nerd-icons collapse git-msg file-time file-size))
  (add-hook 'dired-mode-hook
            (lambda () (progn
                         (setq dired-omit-files (concat dired-omit-files "\\|^\\..*$"))
                         (visual-line-mode -1))))
  (add-hook 'dired-mode-hook #'dired-hide-details-mode #'dired-sort-toggle-or-edit))

(add-hook 'rfn-eshadow-update-overlay-hook #'vertico-directory-tidy)
(file-name-shadow-mode 1)
(setq delete-by-moving-to-trash t
      dired-dwim-target t
      dired-listing-switches "-ahl -v --group-directories-first")

(defun my/dired-delete-permanently ()
  (interactive)
  (let ((delete-by-moving-to-trash nil))
    (dired-do-delete)))

(use-package drag-stuff
  :ensure t)

(use-package flycheck
  :ensure t)

(use-package tramp
  :straight (:type built-in)
  :config
  ;; To speed up connections
  (setq tramp-verbose 0)
  (setq tramp-chunksize 2000)
  (setq tramp-use-ssh-controlmaster-options nil)
  (setq tramp-default-method "ssh"
        tramp-verbose 1
        tramp-default-remote-shell "/bin/sh"
        tramp-connection-local-default-shell-variables
        '((shell-file-name . "/bin/bash")
          (shell-command-switch . "-c")))
  ;; For eglot over Tramp
  (add-to-list 'tramp-remote-path 'tramp-own-remote-path)

  (connection-local-set-profile-variables 'tramp-connection-local-default-shell-profile
                                          '((shell-file-name . "/bin/bash")
                                            (shell-command-switch . "-c"))))

(use-package consult-tramp
  :straight '(consult-tramp
              :type git
              :host github
              :repo "Ladicle/consult-tramp"))

(use-package load-env-vars)

;; Taken from doom emacs

(setq ediff-window-setup-function 'ediff-setup-windows-plain)

(defun my--sudo-file-path (file)
  (let ((host (or (file-remote-p file 'host) "localhost")))
    (concat "/" (when (file-remote-p file)
                  (concat (file-remote-p file 'method) ":"
                          (if-let (user (file-remote-p file 'user))
                              (concat user "@" host)
                            host)
                          "|"))
            "sudo:root@" host
            ":" (or (file-remote-p file 'localname)
                    file))))

(defun my/sudo-find-file (file)
  "Open FILE as root."
  (interactive "FOpen file as root: ")
  (find-file (my--sudo-file-path file)))

(defun my/save-desktop ()
  (interactive)
  (desktop-save user-cache-directory))

(defun my/load-desktop ()
  (interactive)
  (desktop-read user-cache-directory))

(defun my/save-desktop-quit ()
  (interactive)
  (my/save-desktop)
  (save-buffers-kill-terminal))

(defun my/sudo-this-file ()
  "Open the current file as root."
  (interactive)
  (find-file
   (my--sudo-file-path
    (or buffer-file-name
        (when (or (derived-mode-p 'dired-mode)
                  (derived-mode-p 'wdired-mode))
          default-directory)))))

(setq ring-bell-function
      (lambda ()
        (let ((orig-fg (face-foreground 'mode-line)))
          (set-face-foreground 'mode-line "#F2804F")
          (run-with-idle-timer 0.1 nil
                               (lambda (fg) (set-face-foreground 'mode-line fg))
                               orig-fg))))
(defun my/global-escape ()
  "Interactively stop mutli-cursor edits and pop-ups"
  (interactive)
  (cond
   ;; remove highlights
   ((not (null evil-ex-active-highlights-alist)) (evil-ex-nohighlight))
   ;; remove mc
   ((evil-mc-has-cursors-p) (evil-mc-undo-all-cursors))
   ;; close pop-ups (this also closes any buffer in popup-alist)
   ;; to avoid that, use (popper-toggle-type) (SPC w P) on the buffer
   ((not (null popper-open-popup-alist)) (popper-close-latest)))
  (call-interactively #'evil-force-normal-state))

(defun my/append-to-visible-buffer ()
  "Interactively append selected region to a visible buffer. Useful for sending selection to debugger, shell, etc."
  (interactive)
  (let* ((beg (if (region-active-p)
                  (save-excursion (region-beginning))
                (save-excursion (beginning-of-line) (point))))
         (end (if (region-active-p)
                  (save-excursion (region-end))
                (save-excursion (end-of-line) (point))))
         (text (buffer-substring beg end))
         (buf (current-buffer))
         (target-buffer (completing-read "Select buffer to append to: "
                                         (mapcar 'buffer-name (mapcar 'window-buffer (window-list)))))
         )
    (switch-to-buffer target-buffer)
    (goto-char (point-max))
    (insert (concat "\n" text))
    (switch-to-buffer buf)))

;; Always show files as completion candidates
(add-to-list 'completion-at-point-functions 'comint-dynamic-complete-filename)

;; If indentation is possible do it, otherwise show me the completion candidates
(setq tab-always-indent 'complete)
