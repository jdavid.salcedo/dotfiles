(provide 'my-programming-languages)

(use-package python
  :config
  (push 'pyright compilation-error-regexp-alist)
  (push '(pyright "^\\ +\\(.+\\):\\([0-9]+\\):\\([0-9]+\\).+$" 1 2 3) compilation-error-regexp-alist-alist)
  (defun my/restart-python-repl (interpreter)
    "Restart python repl, asking for interpreter if universal argument passed."
    (interactive "P")
    (if (get-buffer "*Python*")
        (let ((kill-buffer-query-functions nil))
          (kill-buffer "*Python*")))
    (if interpreter
        (let ((python-shell-interpreter (read-file-name "Python interpreter: ")))
          (setq last-python-interpreter python-shell-interpreter)
          (message (concat "interpreter: " python-shell-interpreter))
          (run-python))
      (run-python))
    (switch-to-buffer-other-window "*Python*")
    (evil-window-mru))

  (defun my/restart-python-repl-last-interpreter ()
    "Restart python repl with previous interpreter"
    (interactive)
    (if (not (boundp 'last-python-interpreter))
        (message "no previous python interpreter set")
      (progn
        (if (get-buffer "*Python*")
            (let ((kill-buffer-query-functions nil))
              (kill-buffer "*Python*")))
        (let ((python-shell-interpreter last-python-interpreter))
          (message (concat "interpreter: " python-shell-interpreter))
          (run-python))
        (switch-to-buffer-other-window "*Python*")
        (evil-window-mru))))
  )

(use-package dape
  :straight (dape :type git :host github :repo "svaante/dape")
  :config
  (setq dape-repl-use-shorthand t)
  (defun my/ask-for-filename ()
    (interactive)
    (expand-file-name (read-file-name "Run with file")))

  (add-to-list 'dape-configs
               '(streamlit
                 modes (python-ts-mode python-mode)
                 command "python3"
                 command-args ("-m" "debugpy.adapter")
                 :type "executable"
                 :request "launch"
                 :module "streamlit"
                 :cwd dape-cwd-fn
                 :args ["run" my/ask-for-filename]
                 ))

  (add-to-list 'dape-configs
               `(debugpy-remote
                 modes (python-mode python-ts-mode)
                 ensure (lambda (config)
                          (let ((python
                                 (dape--config-eval-value (plist-get config 'command))))
                            (unless (zerop
                                     (call-process-shell-command
                                      (format "%s -c \"import debugpy.adapter\"" python)))
                              (user-error "%s module debugpy is not installed" python))))
                 fn (dape-config-autoport dape-config-tramp)
                 command "python"
                 ;; The important part here is --host
                 command-args ("-m" "debugpy.adapter" "--host" "0.0.0.0" "--port" :autoport)
                 port :autoport
                 host (lambda () (file-remote-p default-directory 'host))
                 :request "launch"
                 :type "executable"
                 :cwd dape-cwd-fn
                 :program my/ask-for-filename
                 :justMyCode nil
                 :showReturnValue t))
  )

(use-package pyvenv
  :init
  (force-mode-line-update t))

(if (featurep 'lsp)
    (progn
      (use-package lsp-pyright
        :ensure t
        :hook (python-mode . (lambda ()
                               (require 'lsp-pyright))))

      (use-package lsp-jedi
        :ensure t)))

(use-package python-isort
  :hook (python-mode . (lambda ()
                         (python-docstring-mode))))
(use-package python-docstring)
(use-package python-black)
(use-package yapfify)
(use-package pip-requirements)
(use-package ein)
(use-package poetry)

(use-package docker)
(use-package dockerfile-mode)
(use-package docker-compose-mode)

(use-package clojure-mode)
(use-package yaml-mode)
