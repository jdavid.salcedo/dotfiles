(provide 'my-looks)

;; Set font based on existing ones
(cond
 ((find-font (font-spec :name "Caskaydia Cove Nerd Font Mono"))
  (set-face-attribute 'default nil :font "Caskaydia Cove Nerd Font Mono" :height 116 :weight 'medium)
  (set-face-attribute 'bold nil :weight 'semi-bold))
 ((find-font (font-spec :name "Hack FC Ligatured"))
  (set-face-attribute 'default nil :font "Hack FC Ligatured" :height 116)
  ))

(use-package doom-themes)
(use-package ef-themes)
(use-package tangonov-theme)
(use-package catppuccin-theme)

(use-package writeroom-mode)

(use-package display-line-numbers
  :init
  (setq display-line-numbers-type 'absolute)
  (global-display-line-numbers-mode))

(use-package rainbow-delimiters
  :ensure t
  :init
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

(use-package nerd-icons-completion
  :ensure t
  :after (marginalia nerd-icons)
  :init
  (nerd-icons-completion-mode)
  :hook
  (marginalia-mode . nerd-icons-completion-mode))

(use-package dashboard
  :ensure t
  :init
  (dashboard-setup-startup-hook)
  :config
  (setq initial-buffer-choice (lambda () (get-buffer-create "*dashboard*")))
  (setq dashboard-center-content t
        dashboard-items '((recents  . 5)
                          (projects . 5)
                          (bookmarks . 5))
        dashboard-icon-type 'nerd-icons
        dashboard-display-icons-p t
        dashboard-set-file-icons t
        dashboard-set-heading-icons t
        dashboard-set-navigator t
        dashboard-set-file-icons t
        dashboard-projects-backend 'project-el
        dashboard-set-footer nil
        dashboard-startup-banner "~/.config/emacs/true.png"
        dashboard-banner-logo-title nil)
  (dashboard-modify-heading-icons '((projects . "nf-oct-project_roadmap")
                                    (bookmarks . "nf-oct-book")
                                    (recents . "nf-oct-file_code"))))

(use-package zoom-frm)

(use-package whitespace
  :init
  (setq whitespace-style '(face tabs tab-mark trailing))
  :hook ((prog-mode text-mode conf-mode) . whitespace-mode))


;; Install necessary packages
(use-package nerd-icons
  :ensure t)

(blink-cursor-mode -1)

(use-package tab-bar
  :init
  (setq tab-bar-show 1
        tab-bar-close-button-show nil
        tab-bar-new-tab-choice "*dashboard*"
        tab-bar-tab-hints t
        tab-bar-new-button-show nil
        tab-bar-format '(tab-bar-format-tabs-groups tab-bar-separator)
        tab-bar-select-tab-modifiers "meta"))

(use-package hide-mode-line)

(use-package doom-modeline
  :init
  (add-hook 'doom-modeline-mode-hook #'size-indication-mode)
  (add-hook 'doom-modeline-mode-hook #'column-number-mode)
  (setq doom-modeline-support-imenu t
        doom-modeline-hud t
        doom-modeline-project-detection t
        doom-modeline-icon t
        doom-modeline-major-mode-icon t
        doom-modeline-major-mode-color-icon t
        doom-modeline-buffer-state-icon t
        doom-modeline-buffer-modification-icon t
        doom-modeline-time-icon t
        doom-modeline-buffer-name t
        doom-modeline-highlight-modified-buffer-name t
        doom-modeline-enable-word-count nil
        doom-modeline-continuous-word-count-modes '(markdown-mode gfm-mode org-mode)
        doom-modeline-buffer-encoding t
        doom-modeline-indent-info nil
        doom-modeline-checker-simple-format t
        doom-modeline-number-limit 99
        doom-modeline-vcs-max-length 12
        doom-modeline-workspace-name t
        doom-modeline-persp-name t
        doom-modeline-persp-icon t
        doom-modeline-lsp t
        doom-modeline-github nil
        doom-modeline-modal t
        doom-modeline-modal-icon t
        doom-modeline-battery t
        doom-modeline-time t
        doom-modeline-display-misc-in-all-mode-lines t
        doom-modeline-env-version t
        doom-modeline-env-load-string "..."
        doom-modeline-buffer-file-name-style 'relative-from-project)
  (doom-modeline-mode 1))

(use-package which-key
  :init
  (setq which-key-sort-order #'which-key-key-order-alpha
        which-key-sort-uppercase-first nil
        which-key-add-column-padding 1
        which-key-max-display-columns nil
        which-key-min-display-lines 6
        which-key-side-window-slot -10)
  (which-key-mode)

  :config
  (setq which-key-idle-delay 0.5
        which-key-popup-type 'minibuffer))

(defalias 'yes-or-no-p 'y-or-n-p)

(use-package winner
  :init
  (winner-mode 1))

(use-package breadcrumb
  :config
  (setq breadcrumb-imenu-max-length 1.0)
  (setq breadcrumb-project-max-length 1.0)

  ;; Make Org heading style the same.
  ;; https://github.com/joaotavora/breadcrumb/issues/35
  (defun breadcrumb-org-crumbs ()
    "Get the chain from the top level heading down to current heading."
    (org-format-outline-path (org-get-outline-path t)
                             (1- (frame-width))
                             nil
                             " > "))
  (defun breadcrumb--header-line ()
    "Helper for `breadcrumb-headerline-mode'."
    (let* ((imenu-crumbs (if (eq major-mode 'org-mode)
                             'breadcrumb-org-crumbs
                           'breadcrumb-imenu-crumbs))
           (x (cl-remove-if
               #'seq-empty-p (mapcar #'funcall
                                     `(breadcrumb-project-crumbs ,imenu-crumbs)))))
      (mapconcat #'identity x (propertize " : " 'face 'breadcrumb-face))))

  (breadcrumb-mode))
;; (use-package breadcrumb
;;   :straight '(breadcrumb
;;               :type git
;;               :host github
;;               :repo "joaotavora/breadcrumb")
;;   :init
;;   (setq breadcrumb-imenu-crumb-separator " ➤ "
;;         breadcrumb-project-max-length 5)
;;   (breadcrumb-mode 1))

(define-minor-mode my/highlight-long-lines
  "Minor mode to highlight lines longer than 80"
  :init-value nil
  :global nil
  :lighter " my/ll"
  (if my/highlight-long-lines
      (highlight-regexp "^.\\{81\\}" 'anzu-match-2)
    (unhighlight-regexp "^.\\{81\\}")))

;; Theming
(set-default 'fill-column 80)

(load-theme 'catppuccin :no-confirm)

(define-minor-mode my/dark-mode
  "Minor mode to enable dark theme"
  :init-value t
  :global t
  :lighter " my/dm"
  (let
      ((dark-theme 'catppuccin)
       (light-theme 'ef-melissa-light))
    (if my/dark-mode
        (progn
          ;;(custom-set-faces
           ;;'(default ((t (:background "gray4"))))
           ;;'(org-block ((t (:background "gray2")))))
          (consult-theme dark-theme))
      (custom-reset-faces
       '(default ((t (:background))))
       '(org-block ((t (:background)))))
      (consult-theme light-theme))))

;; Low-light inactive windows
(defun my/highlight-non-selected-window ()
  "Lowlight other windows with a different background color."
  (let ((alt-color (catppuccin-get-color 'bg-inactive)))
    (walk-windows (lambda (w)
                    (unless (eq w (selected-window))
                      (with-current-buffer (window-buffer w)
                        (buffer-face-set `(:background ,alt-color :weight ,'light))))))
    (buffer-face-set 'default)))
(add-hook 'buffer-list-update-hook 'my/highlight-non-selected-window)

(define-minor-mode my/lowlight-inactive-buffer-mode
  "Minor mode to lowlight the inactive buffers (windows)"
  :init-value t
  :global t
  :lighter " my/li"
  (let ((alt-color (catppuccin-get-color 'bg-inactive)))
    (if my/lowlight-inactive-buffer-mode
        (add-hook 'buffer-list-update-hook 'my/highlight-non-selected-window)
      (remove-hook 'buffer-list-update-hook 'my/highlight-non-selected-window)
      (walk-windows (lambda (w)
                      (with-current-buffer (window-buffer w)
                        (buffer-face-set 'default)))))))

(with-eval-after-load
    (progn
      (my/dark-mode 1)))

(set-fringe-style '(9 . 7))
(setq mouse-yank-at-point t)

(use-package ligature
  :ensure t
  :config
  ;; Enable the "www" ligature in every possible major mode
  (ligature-set-ligatures 't '("www"))
  ;; Enable traditional ligature support in eww-mode, if the
  ;; `variable-pitch' face supports it
  (ligature-set-ligatures 'eww-mode '("ff" "fi" "ffi"))
  ;; Enable all Cascadia Code ligatures in programming modes
  (ligature-set-ligatures 'prog-mode '("|||>" "<|||" "<==>" "<!--" "####" "~~>" "***" "||=" "||>"
                                       ":::" "::=" "=:=" "===" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
                                       "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-->" "---" "-<<"
                                       "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
                                       "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "###" "#_(" "..<"
                                       "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~~" "~@" "~="
                                       "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
                                       "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "=>" "!=" "!!" ">:"
                                       ">=" ">>" ">-" "-~" "-|" "->" "--" "-<" "<~" "<*" "<|" "<:"
                                       "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
                                       "##" "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
                                       "?=" "?." "??" ";;" "/*" "/=" "/>" "//" "__" "~~" "(*" "*)"
                                       "\\\\" "://"))
  ;; Enables ligature checks globally in all buffers. You can also do it
  ;; per mode with `ligature-mode'.
  :init
  (global-ligature-mode t))

(use-package ace-window
  :config
  (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l)))

(use-package indent-guide
  :init
  (add-hook 'prog-mode-hook 'indent-guide-mode)
  (setq indent-guide-char " "
        indent-guide-recursive nil))

(define-minor-mode my/big-mode
  "Big-font mode similar to Doom's"
  :init-value nil
  :global nil
  :lighter " my/bm"
  (if my/big-mode
      (dotimes (i 5)
        (zoom-frm-in))
    (zoom-frm-unzoom)))

(define-minor-mode my/zen-mode
  "Distraction-free mode"
  :init-value nil
  :global nil
  :lighter " my/z"
  (if my/zen-mode
      (progn
        (writeroom-mode)
        (my/big-mode 1)
        (customize-set-variable 'tab-bar-show nil)
        (display-line-numbers-mode -1))
    (writeroom-mode -1)
    (display-line-numbers-mode 1)
    (my/big-mode 0)
    (customize-set-variable 'tab-bar-show t)))

;; Start fullscreen (set =maximized= instead for "just" maximizing)
;; I really don't want this!!
;; (add-to-list 'initial-frame-alist '(fullscreen . fullboth))

;; Better scrolling (emacs 29+)
(when (> emacs-major-version 28)
  (pixel-scroll-precision-mode 1))

;; Enable mouse mode in terminal (and enable system clipboard)
(setq xterm-mouse-mode t
      select-enable-clipboard t)

;; Truncate lines by default
;; (global-visual-line-mode)
(set-default 'truncate-lines t)
