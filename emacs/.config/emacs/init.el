(provide 'init)

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)
(add-to-list 'package-archives
             '("elpa" . "http://elpa.gnu.org/packages/") t)
(add-to-list 'package-archives
             '("org" . "https://orgmode.org/elpa/") t)
(add-to-list 'package-archives
             '("gnu-devel" . "https://elpa.gnu.org/devel/"))
(package-initialize)

;; modules dir
(add-to-list 'load-path (expand-file-name "modules" user-emacs-directory))

;; increase gc threshold to speedup starting up
(setq gc-cons-percentage 0.6)
(setq gc-cons-threshold most-positive-fixnum)

(setq use-package-expand-minimally t
      use-package-always-defer t
      straight-check-for-modifications nil ;;'(find-at-startup)
      )

(run-with-idle-timer 4 nil
                     (lambda ()
                       (setq gc-cons-threshold  67108864) ; 64M
                       (setq gc-cons-percentage 0.1) ; original value
                       (garbage-collect)))

;; Set transparency
(set-frame-parameter nil 'alpha-background 100)
(add-to-list 'default-frame-alist '(alpha-background . 100))

(add-to-list 'desktop-locals-to-save 'buffer-undo-list)

;; Load the main modules
(require 'my-core)
(require 'my-utils)
(require 'my-looks)
(require 'my-editing)
(require 'my-project)
(require 'my-programming)
;;(require 'my-programming-languages)
(require 'my-shell)
(require 'my-org)
(require 'my-browser)
(require 'my-keybinds)
(require 'my-copilot)
