//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/		/*Command*/		/*Update Interval*/		/*Update Signal*/

	{ "",			"net",				10,			0 },

	{ "",			"battery",			10,			0 },

	{ "",			"backlight2",			0,			6 },

	{ "",			"volume",			0,			10},
	
	{ "",			"dte",				1,			0 },
	
	{ "",			"clock",			1,			0 }
};

// sets delimeter between status commands. NULL pointer means no delimeter.
static const char *delim = "  ";
